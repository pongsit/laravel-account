<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Account\Http\Controllers\AccountController;

Route::prefix('account')->middleware(['userCan:manage_account'])->group(function () {
	Route::post('/store', [AccountController::class, 'store'])->name('account.store');
	Route::post('{account:slug}/statement', [AccountController::class, 'statement'])->name('account.statement');
	Route::post('{account:slug}/statement/store', [AccountController::class, 'statementStore'])->name('account.statement.store');
	Route::get('{account:slug}/statement/create', [AccountController::class, 'statementCreate'])->name('account.statement.create');
	Route::get('/statement/show/{statement:slug}', [AccountController::class, 'statementShow'])->name('account.statement.show');
});