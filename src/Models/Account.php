<?php

namespace Pongsit\Account\Models;

use Pongsit\Account\Database\Factories\AccountFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Pongsit\Account\Models\Statement;
use Pongsit\Photo\Models\Album;

class Account extends Model
{
  use HasFactory;
  
  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  // ไว้บันทึก accountable_type และ accountable_id
  public function accountable()
  {
      return $this->morphTo();
  }

  public function albums(){
      return $this->morphMany(Album::class,'albumable');
  }

  // public function statements(){
  //     return $this->morphMany('Pongsit\Account\Models\Statement', 'statementable');
  // }

  public function statements(){
      return $this->hasMany(Statement::class);
  }
}
