<?php

namespace Pongsit\Account\Models;

use Pongsit\Account\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  // public function statementable(){
  //     return $this->morphTo();
  // }

  public function account(){
      return $this->belongsTo(Account::class);
  }

  public function album()
  {
      return $this->belongsTo('Pongsit\Photo\Models\Album');
  }
  
}
