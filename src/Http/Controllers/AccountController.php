<?php

namespace Pongsit\Account\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use Pongsit\User\Models\User;
use Throwable;
use Image;
use Pongsit\Account\Models\Account;
use Pongsit\Photo\Models\Album;
use Pongsit\System\Models\System;
use Pongsit\Account\Models\Statement;

class AccountController extends Controller
{
	public function index(){
		// $variables['abilities'] = Account::where('status',1)->orderBy('power','desc')->get();
		// return view('Account::index',$variables);
	}

	public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ],[
            'name.required'=>'กรุณากรอกชื่อแสดง',
        ]);

        $infos = [];
        $infos['name'] = $request->name;

        if(!empty($request->description)){
            $infos['description'] = $request->description;
        }

        if(isset($request->status)){
            $infos['status'] = $request->status;
        }

        if(!empty($request->id)){
            $account = Account::find($request->id);
            $account->update($infos);
        }else{
        	$account = new Account($infos);
            $account->unique = unique();
            $account->slug = slug($account, 'unique');
            $account->save();
        }

        return back()->with(['success'=>'เรียบร้อย']);
    }

    public function statementStore(Request $request, Account $account)
    {

        $request->validate([
            'detail' => 'required',
            'album_id' => 'required',
        ],[
            'detail.required'=>'กรุณากรอกรายละเอียด',
            'album_id.required'=>'หลักฐานผิดพลาดกรุณาติดต่อ admin',
        ]);

        if($request->cd == 'credit'){
            if(empty($request->credit)){
                return back()->with(['error'=>'กรุณากรอกช่องรายรับครับ']);
            }
            if(!empty($request->debit)){
                $request->debit = 0;
            }
        }

        if($request->cd == 'debit'){
            if(empty($request->debit)){
                return back()->with(['error'=>'กรุณากรอกช่องรายจ่ายครับ']);
            }
            if(!empty($request->credit)){
                $request->credit = 0;
            }
        }

        $album = Album::find($request->album_id);
        if(empty($album->getPhotos('vault'))){
            return back()->with(['error'=>'กรุณาแนบหลักฐานครับ']);
        }

        $latest_statement = $account->statements()->latest()->first();
        $latest_statement_credit_total = 0;
        $latest_statement_debit_total = 0;
        $latest_statement_total = 0;
        if(!empty($latest_statement)){
            $latest_statement_credit_total = $latest_statement->credit_total;
            $latest_statement_debit_total = $latest_statement->debit_total;
            $latest_statement_total = $latest_statement->total;
        }

        $statement = new Statement;

        $statement->unique = unique();
        $statement->slug = slug($statement, 'unique');

        $statement->album_id = $request->album_id;

        $statement->credit = 0;
        if(!empty($request->credit)){
            $statement->credit = $request->credit;
            $statement->credit_total = $statement->credit + $latest_statement_credit_total;
        }

        $statement->debit = 0;
        if(!empty($request->debit)){
            $statement->debit = $request->debit;
            $statement->debit_total = $statement->debit + $latest_statement_debit_total;
        }  

        $statement->total = $latest_statement_total + $statement->credit - $statement->debit;  

        $statement->detail = $request->detail;

        if(!empty($request->description)){
            $statement->description = $request->description;
        }
        $statement->user_id = user()->id;

        $account->statements()->save($statement);

        return redirect(route('account.statement.show',$statement))->with(['success'=>'เรียบร้อย']);
    }

    public function statementCreate(Account $account)
    {
        $variables['account'] = $account;
        if(!$account->albums()->where('name','proof')->exists()){
            $album = new Album(['name'=>'proof','user_id'=>$account->user_id]);
            $album->unique = unique();
            $album->slug = slug($album,'unique');
            $account->albums()->save($album);
        }else{
            $album = $account->albums()->where('name','proof')->orderBy('id','desc')->first();

            if($account->statements()->where('album_id',$album->id)->exists()){
                $album = new Album(['name'=>'proof','user_id'=>$account->user_id]);
                $album->unique = unique();
                $album->slug = slug($album,'unique');
                $account->albums()->save($album);
            }
        }
        $variables['album'] = $album;
        $album->slotPhotoSync();
        return view('account.statement.create',$variables);
    }

    public function statementShow(Statement $statement)
    {
        $variables['statement'] = $statement;
        return view('account.statement.show',$variables);
    }


}
