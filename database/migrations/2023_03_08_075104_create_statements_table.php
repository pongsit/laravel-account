<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {
            $table->id();
            $table->string('unique');
            $table->string('slug')->unique();
            $table->string('detail');
            $table->string('credit')->default(0);
            $table->string('credit_total')->default(0);
            $table->string('debit')->default(0);
            $table->string('debit_total')->default(0);
            $table->string('total')->default(0);
            $table->bigInteger('album_id');
            $table->bigInteger('account_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
